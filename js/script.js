$(document).ready(function(){
  $('.imageBox').hide();
  $('.portfolioContent').hide();
  $('.portfolioHeader').on('click', function(){
    var content = $(this).parent().find('.portfolioContent');
    if($(content).is(':visible')){
      $(content).slideUp(300);
    } else {
      $(content).slideDown(300);
    }
  });

  $('.portfolioContent .images img').on('click', function(){
    var imageBox = $('.imageBox');
    console.log($(this).attr('src'));
    if($(imageBox).is(':visible')){
      $(imageBox).fadeOut(300);
    } else {
      $(imageBox).find('img').attr('src', $(this).attr('src'));
      $(imageBox).fadeIn(300);
    }
  });

  $('.imageBox').on('click', function(e){
    if($(e.target).attr('class') == 'imageBox'){
      $('.imageBox').fadeOut(300);
    }
  });
});


//http://stackoverflow.com/questions/11760898/jquery-get-the-element-thats-on-the-middle-of-the-visible-screen-while-scroll
// Way past cool, edited for use here
var findMiddleElement = (function(docElm){
    var elements = $('section');

    return function(e){
        var middleElement;
        if( e && e.type == 'resize' )
            viewportHeight = docElm.clientHeight;

        elements.each(function(){
            var pos = window.pageYOffset;
            var top = $(this).offset().top - 200;
            var bottom = $(this).offset().top + $(this).height();

            if( pos > top && pos < bottom ){
                middleElement = this;
                console.log(this);
                return false; // stop iteration
            }
        });
        if(middleElement != undefined){
          changeNavColour($(middleElement).attr('id'));
        }
    }
})(document.documentElement);

$(window).on('scroll resize', findMiddleElement);

function changeNavColour(toChange){
  $('nav').attr('class', toChange);
}
